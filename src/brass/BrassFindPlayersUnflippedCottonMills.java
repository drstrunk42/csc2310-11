package brass;

import java.util.List;
import java.util.ArrayList;

public class BrassFindPlayersUnflippedCottonMills implements util.FindCommand<BrassCity>
{
	private int player_id;
	private List<Integer> unflipped;
	
	public BrassFindPlayersUnflippedCottonMills(int player_id)
	{
		this.player_id = player_id;
		
		unflipped = new ArrayList<Integer>();
	}
	
	
	public void execute(BrassCity brass_city)
	{
		int count = 0;
		
		util.CountCommand<BrassIndustry> count_unflipped = new BrassCountPlayersUnflippedIndustry(2, player_id);
		brass_city.execute(count_unflipped);
		count = count_unflipped.getCount();
		
		while(count > 0)
		{
			int city_id = brass_city.getCityID();
			
			unflipped.add(city_id);
			
			count--;
		}
	}
	
	
	public List<Integer> getList()
	{
		return unflipped;
	}
	
}