package brass;

import java.util.List;
import java.util.Iterator;

class BrassDepthLimitedConnectionSearch extends BrassConnectionSearch
{
	private int depth_limit;
	
	public BrassDepthLimitedConnectionSearch(int depth_limit, List<BrassConnection> connections)
	{
		super(connections);
		
		this.depth_limit = depth_limit;
	}
	
	public table.TableInterface<Integer, Integer> connectionSearch(int start_city_id, table.Comparator<Integer, Integer> comp_city_ids)
	{
		table.TableInterface<Integer, Integer> generated_table = table.TableFactory.createTable(comp_city_ids);
		
		if(depth_limit > 0)
		{
			depthLimitedRec(start_city_id, 1, depth_limit, generated_table);
		}
		
		return generated_table;
	}
	
	///////////////////////////////////////////////////////////////
	private void depthLimitedRec(int start_city, int curr_depth, int depth_limit, table.TableInterface<Integer, Integer> explored)
	{
		Iterator<BrassConnection> it = connections.iterator();
		
		while(it.hasNext())
		{
			BrassConnection connection = it.next();
			if(connection.isLinkConstructed())
			{
				int[] connected_cities = connection.getConnectedCities();
				int test_city = isLinkConnected(start_city, connected_cities);
				if(test_city == 0)
				{
					continue;
				}
				try
				{
					explored.tableInsert(new Integer(test_city));
					if(curr_depth < depth_limit)
					{
						depthLimitedRec(test_city, curr_depth+1, depth_limit, explored);
					}
				}
				catch(table.TableException exception)
				{
					//do nothing here because we don't care.
				}
			}
		}
	}
} 