package brass;

import java.util.List;

abstract class BrassConnectionSearch implements BrassConnectionSearchInterface
{
	public abstract table.TableInterface<Integer, Integer> connectionSearch(int start_city_id, table.Comparator<Integer, Integer> comp_city_ids);
	protected List<BrassConnection> connections;
	
	public BrassConnectionSearch(List<BrassConnection> connections)
	{
		connections = connections;
	}
	
	public int isLinkConnected(int city_id, int[] connected_cities)
	{
		int curr_city = 0;
		
		if(connected_cities[0] == city_id)
		{
			curr_city = connected_cities[1];
		}
		
		if (connected_cities[1] == city_id)
		{
			curr_city = connected_cities[0];
		}
		
		return curr_city;
	}
	
} ///////////////////////////////////////////