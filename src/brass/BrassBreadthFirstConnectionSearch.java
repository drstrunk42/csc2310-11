package brass;

class BrassBreadthFirstConnectionSearch extends BrassConnectionSearch
{
	public BrassBreadthFirstConnectionSearch(java.util.List<BrassConnection> connections)
	{
		super(connections);
	}
	
	public table.TableInterface<Integer, Integer> connectionSearch(int start_city_id, table.Comparator<Integer, Integer> comp_city_ids)
	{
		util.QueueLinked<Integer> queue = new util.QueueLinked<Integer>();
		queue.enqueue(new Integer(start_city_id));

		table.TableInterface<Integer, Integer> table_interface = table.TableFactory.createTable(comp_city_ids);
		table_interface.tableInsert(new Integer(start_city_id));
		
		while(queue.size() > 0)
		{
			int next = queue.dequeue();
		}
		
		return table_interface;
	}
	
}